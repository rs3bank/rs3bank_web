FROM node:11.11-alpine

EXPOSE 3000

ADD /package.json ./rs3_bank/package.json
ADD /package-lock.json ./rs3_bank/package-lock.json
WORKDIR /rs3_bank
RUN npm install

ADD ./ /rs3_bank/

RUN npm run build

CMD npm start
