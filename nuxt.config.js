const pkg = require("./package");
const {
  i18n_config,
  yandex_metrica_config,
  validate_config,
  redirect_config
} = require("./config/nuxt_modules");

module.exports = {
  ssr: true,
  telemetry: false,
  head: {
    title: pkg.name,
    meta: [{
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content: pkg.description
      }
    ],
    link: [{
      rel: "icon",
      type: "image/x-icon",
      href: "/favicon.ico"
    }]
  },

  loading: {
    color: "#2fce2f"
  },

  css: [
    "~/assets/styles/global",
    // "~/assets/styles/rs3"
  ],

  // Plugins to load before mounting the App
  // plugins: [],

  modules: [
    "@nuxtjs/pwa",
    "@nuxtjs/style-resources",
    ["@nuxtjs/yandex-metrika", yandex_metrica_config],
    ['@nuxtjs/redirect-module', redirect_config],
    "@nuxtjs/axios",

    "bootstrap-vue/nuxt",
    ["nuxt-i18n", i18n_config],
    ["nuxt-fontawesome", {
      component: "fa"
    }],
    ["nuxt-validate", validate_config],
    '~/plugins/coffeescript.js'
  ],

  build: {
    // You can extend webpack config here
    extend(config, ctx) {}
  }
};
