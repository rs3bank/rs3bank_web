const i18n_config = {
  locales: [{
      code: "rus",
      iso: "ru-RU",
      name: "Русский",
      file: "rus.json"
    },
    {
      code: "eng",
      iso: "en-US",
      name: "English",
      file: "eng.json"
      /* },
      {
        code: "spa",
        iso: "es-ES",
        name: "Español",
        file: "spa.json"*/
    }
  ],
  lazy: true,
  langDir: "locales/",
  strategy: "prefix_and_default",
  defaultLocale: "rus",
  fallbackLocale: "rus",
  detectBrowserLanguage: {
    cookieKey: "rs3bank_i18n_redirected",
    fallbackLocale: "rus"
  },
  parsePages: false
};

const yandex_metrica_config = {
  id: "52832857",
  webvisor: true,
  clickmap: true,
  useCDN: false,
  trackLinks: true,
  accurateTrackBounce: true
};

const validate_config = {
  inject: true,
  fieldsBagName: 'veeFields',
  errorBagName: 'veeErrors'
}

const redirect_config = [{
    from: '^/text(|s)$',
    to: '/text/all'
  },
  {
    from: '^/rus/text(|s)$',
    to: '/rus/text/all'
  },
  {
    from: '^/eng/text(|s)$',
    to: '/eng/text/all'
  },
]

module.exports = {
  i18n_config,
  yandex_metrica_config,
  validate_config,
  redirect_config
};
